import json
import requests
from datetime import datetime
from bs4 import BeautifulSoup

t = datetime.utcnow() #змінна для збереження поточного часу
payload = {'username': 'кассирльвов7', 'password': '1234'}

def check_balance (uid):
    s = requests.Session()
    payload1 = {'uid': uid}
    r = s.post('http://10.4.4.249:8080/login', params = payload)
    r = s.get('http://10.4.4.249:8080/define-card', params = payload1)
    
    soup = BeautifulSoup(r.text, "html.parser")
    str = soup.find(attrs={'id': 'card-balance'})
    if str.string == None:
        balance_in = 0
    else:
        balance_in = float(str.string)
    return (balance_in, r)
    
def payout (uid, amount):
    t = datetime.utcnow()
    s = requests.Session()
    payload1 = {'uid': uid, 'amount': amount, 'action': 'payout', 'transaction': t}
    r = s.post('http://10.4.4.249:8080/login', params=payload)
    r = s.post('http://10.4.4.249:8080/card-operation', params = payload1)
    return(r)

def deposit (uid, amount):
    t = datetime.utcnow()
    s = requests.Session()
    payload1 = {'uid': uid, 'amount': amount, 'action': 'activation', 'transaction': t}
    r = s.post('http://10.4.4.249:8080/login', params=payload)
    r = s.post('http://10.4.4.249:8080/card-activation', params=payload1)
    return(r)

def refill (uid, amount):
    t = datetime.utcnow()
    s = requests.Session()
    payload1 = {'uid': uid, 'amount': amount, 'action': 'refill', 'transaction': t}
    r = s.post('http://10.4.4.249:8080/login', params=payload)
    r = s.post('http://10.4.4.249:8080/card-operation', params=payload1)
    return(r)

def log(operations, r, uid, balance):
    t = datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')
    log_string = (t + ': ' + operations + ': ' + str(r.status_code) + ': ' + str(uid) + ': ' + str(balance) + '\n')
    client_file = open('log.txt', 'a')
    client_file.write(log_string)
    client_file.close()
    return

def log_recive(pin, rfid, operation, summ):
    t = datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')
    log_string = (t + ': ' + operation + ': ' + str(rfid) + ': ' + str(summ) + '\n')
    client_file = open('log_recive.txt', 'a')
    client_file.write(log_string)
    client_file.close()
    return

