import socket
import json
import requests
from bs4 import BeautifulSoup
from datetime import datetime
from function import log, log_recive 
from function import check_balance, payout, deposit, refill
pincodes = {1788, 3298, 6678}

sock = socket.socket()
sock.bind(('', 9090))
while True:
    sock.listen(100)
    conn, addr = sock.accept()
    data = conn.recv(10240)
    udata = data.decode('utf-8')
    if not data:
        break

    z = udata.split(';')
    pin = int(z[0])
    rid = str(z[1])
    rfid = int(rid[18]+rid[19]+rid[15]+rid[16]+rid[12]+rid[13]+rid[9]+rid[10]+rid[6]+rid[7]+rid[3]+rid[4]+rid[0]+rid[1], 16)
    operation = str(z[2])
    if len(z[3]) == 0 :
        summ = 0
    else:
        summ = int(z[3])*100
    log_recive(pin, rfid, operation, summ)
   
    if pin in pincodes:
        if operation == 'b':
            balance, r = check_balance(rfid)
            send_data = str(str(balance)+';;')
            conn.send(send_data.encode('utf-8'))
            log('Check balance    ', r, rfid, balance)
        if operation == 'w':
            balance, r = check_balance(rfid)
            log('Check balance    ', r, rfid, balance)
            if balance == 0:
                log('Withdraw error   ', r, rfid, balance)
                conn.send(b';err, balance 0;')
            else:
                r = payout(rfid, int(balance*100))
                #send_data = str(str(balance)+';;')
                #conn.send(send_data.encode('utf-8'))
                log('Withdraw         ', r, rfid, balance)
        if operation == 'd':               
            balance, r = check_balance(rfid)
            log('Check balance    ', r, rfid, balance)
            if balance == 0:
                r = deposit(rfid, summ)
                log('Deposit          ', r, rfid, balance)
            else:
                r = refill(rfid, summ)
                log('Refill           ', r, rfid, balance)
        balance, r = check_balance(rfid)
        log('Check balance    ', r, rfid, balance)
        send_data = str(str(balance)+';;')
        conn.send(send_data.encode('utf-8'))
    else:
        conn.send(b'Err;Wrong PIN')
